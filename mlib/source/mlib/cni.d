/*
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is herby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PEFORMANCE OF THIS SOFTWARE.
 */

/**
 * A D module for parsing [CNI] configuration files.
 *
 * Future ideas would be to indicate where in the file
 * an error occurred (file:line/character).
 *
 * Compliance:
 *
 *  * `core`: 29/29, `ini`: compliant, `ext`: more-keys
 *
 * Author: nemophila
 * Date:
 * Homepage: https://osdn.net/users/nemophila/pf/mlib
 * License: 0BSD
 * Standards: [CNI] 0.1.0
 * Version: 0.1.0
 *
 * History:
 *      0.1.0 The initial version
 *
 * Bugs:
 *  - Doesn't implement the "suggested API".
 *
 * [CNI]: https://www.github.com/libuconf/cni
 */
module mlib.cni;
private:

import std.uni : isWhite;
import std.typecons : nullable, Nullable;
import std.stdio;
import std.string : strip;

/**
 * Options for configuring how Cni.fromString will parse the provided
 * string.
 *
 * The options are:
 *
 *  * `moreKeys` which enables the [more-keys] extension, allowing
 *    for almost any character to be a valid part of a key name.
 *
 *  * `iniCompatibility` provides compatibility with INI files by
 *    allowing comments to start with a semi-colon (`;`).
 */
public struct CniOptions
{
    /**
     * If this extension is used, keys may be composed of any characters which are not whitespace,
     * `#`, `=`, `[`, `]`, or '`'.
     *
     * The meaning and restrictions of dots remain unchanged.
     */
    bool moreKeys = false;
    /**
     * If this extension is used, comments may also begin with `;`.
     */
    bool iniCompatibility = false;
}

/**
 * Scanner structure for parsing CNI files.
 *
 * There is no real purpose for declaring a CniParser,
 * the indended use is to call CniParser.fromString.
 */
public struct CniParser
{

private:
    /**
     * Parses CNI formatted text and returns the resulting associative array.
     *
     * Params:
     *  text = The CNI formatted text to parse.
     *  options = Parser options
     *
     * Throws: `Exception` on any errors.
     */
    public static string[string] fromString(string text, CniOptions options = CniOptions.init)
    {
        CniParser parser = CniParser(text, options);
        string[string] map = parser.parse();

        return map;
    }

    string[string] parse()
    {
        skipUntil((c) => false == isWhite(c) && false == skipComment(c));

        parseTopLevel();

        return this.map;
    }

    /**
     * Skip a comment until end of line, along with any extra whitespace following
     * end of line.
     *
     * ```d
     * // Continue skipping over comments and related whitespace
     * // until isComment returns false.
     * while (true == skipComment(currentChar())) { }
     * ```
     *
     * Returns: `false` if the current character is **not** a comment character.
     *          Otherwise, it'll return `true` that something has been skipped
     *          (and you may need to skip more for multiline comments).
     */
    bool skipComment(dchar c) {
        if (false == isComment(c)) {
            return false;
        }

        skipUntil((c) => true == isVerticalWhitespace(c));

        return true;
    }

    void parseTopLevel()
    {
        skipUntil((c) => false == isWhite(c) && false == skipComment(c));

        if (endOfInput()) {
            return;
        }

        if (currentChar() == '[')
        {
            moveForward(1); // [
            parseSection();
            parseTopLevel();
            return;
        }

        // About to parse a key-(raw)value
        if (false == isValidKeyChar(currentChar())) {
            throw new Exception("Invalid key name, cannot start with: " ~ currentChar());
        }

        marker = cursor;
        skipUntil((c) => false == isValidKeyChar(c));
        string key = text[marker..cursor];
        assert(isValidKey(key), "Invalid key " ~ key);

        /* https://github.com/libuconf/cni/blob/v0.1.0/SPEC.md#key-rawvalue-pairs
         * It's a key, an equals, and a value, with arbitrary amounts of whitespace
         * before and after the `=`.
         */
        skipUntil((c) => c == '='); // error here, because there is no =!
        if (endOfInput()) {
            throw new Exception("Invalid CNI syntax: No '=' separating key and value.");
        }
        moveForward(1); // move past '='
        skipUntil((c) => false == isWhite(c));

        // RAW values. caps not required.
        // "A rawvalue starts with a backtick and ends with a backtick, but may
        //  contain any UTF-8 characters besides those. If a backtick needs to
        //  be inserted into a raw value, a backtick may be escaped with a
        //  second one".
        if (currentChar() == '`') {
            moveForward(1);
            inRawValue = true;
        }

        marker = cursor;

        string value;

        if (inRawValue) {
            import std.array : appender;
            import std.string : replace;

            auto app = appender!string;

            while (!endOfInput()) {
                if (currentChar() == '`' && tryPeek().isNull) {
                    break;
                } else if (tryPeek().isNull()) {
                    throw new Exception("Reached EOF while parsing rawvalue");
                }
                // escape `` -> `
                if (currentChar() == '`' && tryPeek('`')) {
                    moveForward(2);
                    app ~= '`';
                    continue;
                } else if (currentChar() == '`') {
                    // a standalone `
                    break;
                }

                app ~= currentChar();
                moveForward(1);
            }

            value = app.data();
            moveForward(1); // move past ending `
            inRawValue = false;
        } else {
            skipUntil((c) => isVerticalWhitespace(c) || isComment(c));
            value = text[marker..cursor].strip();
        }

        // "Keys may contain dots, but may not start or end with them"
        if (key[0] == '.' || key[$ - 1] == '.') {
            throw new Exception("Invalid key name: starts or ends with dot.");
        }

        if (currentSection != "") {
            map[currentSection ~ "." ~ key] = value;
        } else {
            map[key] = value;
        }

        parseTopLevel();
    }

    Nullable!char tryPeek() {
        Nullable!char value;

        if (endOfInput() || (cursor + 1) >= text.length) {
            return value;
        }

        value = text[cursor + 1];
        return value;
    }

    bool tryPeek(char c) {
        if (endOfInput() || (cursor + 1) >= text.length) {
            return false;
        }
        return text[cursor + 1] == c;
    }

    void parseSection()
    {
        import std.ascii : isAlphaNum;

        // At this point, we should be just after the opening
        // square bracket.

        /*
         * Whitespace is not actually significant
         * this allows some weird things to happen
         * i.e:
         * [
         *        base
         * ]
         */
        skipUntil((c) => false == isWhite(c));

        // If there is no actual key name (i.e. [<whitespace>]),
        // then the section name should be blank.
        if (currentChar() == ']') {
            moveForward(1); // ]
            currentSection = "";
            return;
        }
        if (false == isValidKeyChar(currentChar())) {
            throw new Exception("Invalid section name. Cannot start with: " ~ currentChar());
        }

        marker = cursor;

        skipUntil((c) => false == isValidKeyChar(c));
        // Trailing whitespace after valid section name
        skipUntil((c) => false == isWhite(c));

        if (currentChar() != ']') {
            throw new Exception("Invalid key");
        }

        currentSection = text[marker .. cursor].strip();
        if (currentSection[0] == '.' || currentSection[$ - 1] == '.') {
            throw new Exception("Invalid section name. Cannot start or end with dot.");
        }

        moveForward(1); // ]
    }

    bool isValidKeyChar(dchar ch)
    {
        /*
         * "If [more-keys] extension is used, keys may be composed of
         *  any characters which are not whitespace, #, =, [, ], or `"
         */
        if (options.moreKeys)
        {
            return (!isWhite(ch) && !isComment(ch) &&
                !(ch == '=' || ch == '[' || ch == ']' || ch == '`'));
        }
        else
        {
            import std.ascii : isAlphaNum;

            return isAlphaNum(ch) || (ch == '.' || ch == '_' || ch == '-');
        }
    }

    bool isValidKey(string key)
    {
        bool ret = true;
        foreach (c; key)
        {
            if (false == isValidKeyChar(c))
            {
                ret = false;
                break;
            }
        }

        return ret;
    }

    char previousChar() {
        return text[cursor - 1];
    }

    char currentChar()
    {
        return text[cursor];
    }

    void moveForward(size_t positions = 1)
    {
        cursor += positions;
    }

    void moveBackwards(size_t positions = 1)
    {
        cursor -= positions;
    }

    bool endOfInput()
    {
        return this.cursor >= this.text.length;
    }

    void skipUntil(bool delegate(char c) shouldBreak)
    {
        while (!endOfInput() && false == shouldBreak(currentChar()))
        {
            moveForward(1);
        }
    }

    bool isComment(dchar c)
    {
        if (options.iniCompatibility)
        {
            return c == '#' || c == ';';
        }
        return c == '#';
    }

    bool isVerticalWhitespace(dchar ch)
    {
        return (ch == '\n' || ch == '\u000B' || ch == '\u000C' ||
                ch == '\r' || ch == '\u0085' || ch == '\u2028' ||
                ch == '\u2029');
    }

    string text;
    CniOptions options;
    size_t cursor;
    size_t marker;
    string currentSection;
    bool inRawValue;

    string[string] map;
}

// This version should only be used for testing against
// the canonical set of tests for CNI:
//
//   https://github.com/libuconf/cni/tree/v0.1.0/tests
//
// run the compiled program pointing to the directory of
// entries to run.
version(CNITesting):

import std.file;
import std.json;
import std.path;
import std.string;

bool canParse(string path, CniOptions options) {
    string rawCni = readText(path);

    try {
        CniParser.fromString(rawCni, options);
    } catch (Exception e) {
        return false;
    }
    return true;
}

bool matchesJson(string path, CniOptions options) {
    string jsonPath = path.setExtension(".json");
    JSONValue json;
    string[string] cni;

    {
        scope rawJson = readText(jsonPath);
        json = parseJSON(rawJson);

        scope rawCni = readText(path);
        cni = CniParser.fromString(rawCni, options);
    }

    foreach(key, value; json.objectNoRef) {
        if (json[key].str != cni[key]) {
            return false;
        }
    }

    return true;
}

void testDir(string path, CniOptions options) {
    string currentSetName = baseName(path);

    size_t numberOfTests = 0;
    size_t passed = 0;

    foreach(DirEntry entry; dirEntries(path, SpanMode.breadth)) {
        if (entry.isDir()) {
            continue;
        }

        if (entry.name().endsWith(".json")) {
            continue;
        }

        numberOfTests += 1;
        string testName = baseName(dirName(entry.name)) ~ "/" ~ baseName(entry.name);
        writef("%s %02d - %s: ", currentSetName, numberOfTests, testName);

        if (entry.name().endsWith("_fail.cni")) {
            if (false == canParse(entry.name(), options)) {
                writeln("Success");
                passed += 1;
                continue;
            }
            writeln("Failure");
            continue;
        }

        if (true == matchesJson(entry.name(), options)) {
            passed += 1;
            writeln("Success");
        } else {
            writeln("Failure");
        }
    }

    writefln("%s: %d/%d", currentSetName, passed, numberOfTests);
}

int main(string[] args) {
    if (args.length < 2) {
        stderr.writefln("usage: %s <path> [options]\n", args[0]);
        stderr.writeln("<path> can be either a directory containing tests,");
        stderr.writeln("or it can be a singular .cni file. A JSON file is");
        stderr.writeln("expected to be located next to the .cni file.");
        stderr.writeln("\nFor a single file, there are a couple of options:");
        stderr.writeln("  --more-keys        enable more-keys extension");
        stderr.writeln("  --ini              enable ini compatibility");
        return 1;
    }

    bool moreKeys = false;
    bool ini = false;

    foreach(opt; args) {
        if ("--more-keys" == opt)
            moreKeys = true;
        if ("--ini" == opt)
            ini = true;
    }

    if (isFile(args[1])) {
        CniOptions options = CniOptions(moreKeys, ini);

        if (args[1].endsWith("_fail.cni")) {
            if (canParse(args[1], options)) {
                stderr.writeln("_fail test failed to produce parsing error");
                return 1;
            }
            writeln("_fail test correctly produced error!");
            return 0;
        } else {
            if (matchesJson(args[1], options)) {
                writeln("Correctly parsed and matched CNI with JSON");
                return 0;
            }

            stderr.writeln("Failed to parsed and match CNI with JSON");
            return 1;
        }
    }

    string baseDir = args[1];
    string bundleDir = buildPath(baseDir, "bundle");
    string coreDir = buildPath(baseDir, "core");
    string extDir = buildPath(baseDir, "ext");
    string iniDir = buildPath(baseDir, "ini");

    testDir(coreDir, CniOptions(false, false));
    testDir(extDir, CniOptions(true, false));
    testDir(iniDir, CniOptions(false, true));

    // These tests are a bit larger, so we'll keep them separate.
    testDir(bundleDir, CniOptions(false, true));

    return 0;
}
