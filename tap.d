/*
 * Copyright (C) 2022 mio <stigma@disroot.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is herby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PEFORMANCE OF THIS SOFTWARE.
 */

/**
 * An TAP producing library for D.
 *
 * License: 0BSD
 * Standards: TAP Specification 12
 * Version: 0.2
 * History:
 *      0.2 Add `bool pass(string)`, `bool is_not(T, U)(T, U, string)`.
 *          `ok` now returns bool (the value of `result`).
 *      0.1 Initial release
 */
module tap;

import core.stdc.stdlib : exit, EXIT_FAILURE;
import std.stdio : File, stdout, stderr;
import std.string : splitLines;

private bool planSet = false;
private bool noPlan = false;
private bool skipAll = false;
private size_t plannedTests = 0;
private size_t executedTests = 0;
private size_t failedTests = 0;

private File outstream;
private File errstream;

public enum TAPPlan : byte
{
    noPlan = -1,
    skipAll = -2,
}

shared static this()
{
    outstream = stdout;
    errstream = stderr;
}

public void diag(in string msg)
{
    if (msg !is null) {
        string[] lines = splitLines(msg);
        foreach(line; lines) {
            outstream.writefln("# %s", line);
        }
    }
}

public void doneTesting()
{
    if (false == planSet)
        die("No tests were planned!");

    if (false == skipAll && true == noPlan)
        printPlan(executedTests);
}

public bool fail(string message)
{
    return ok(false, message);
}

public bool is_not(T, U)(T got, U expected, string message)
{
    try {
        return ok(got != expected, message);
    } catch (Exception e) {
        fail(message);
        diag("In test " ~ message);
        diag("Caught exception: " ~ e.msg);
        return false;
    }
}

public bool ok(bool result, string name)
{
    if (false == planSet)
        die("No plan set but trying to run tests.  Use plan(size_t) first.");

    executedTests += 1;

    if (false == result) {
        errstream.write("not ");
        failedTests += 1;
    }

    outstream.writef("ok %d", executedTests);

    if (null !is name)
        outstream.writef(" - %s", name);

    outstream.write("\n");
    outstream.flush();

    return result;
}

public void plan(size_t numberOfTests)
{
    if (true == planSet)
        die("You tried to plan twice!");

    if (0 == numberOfTests)
        die("You can't run 0 tests!");

    printPlan(numberOfTests);
    plannedTests = numberOfTests;
    planSet = true;
}

public void plan(TAPPlan type)
{
    if (true == planSet)
        die("You tried to plan twice!");

    planSet = true;
    noPlan = (type == TAPPlan.noPlan);
    skipAll = (type == TAPPlan.skipAll);
}

public bool pass(string message)
{
    return ok(true, message);
}

public void setTapOutstream(File outstream)
{
    .outstream = outstream;
}

public void setTapErrstream(File errstream)
{
    .errstream = errstream;
}

private void die(in string msg)
{
    errstream.writeln(msg);
    exit(EXIT_FAILURE);
}

private void printPlan(size_t numberOfTests)
{
    outstream.writefln("1..%d", numberOfTests);
}
