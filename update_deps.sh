#!/bin/sh
#
# This script assumes it's being run in the medialist-cli directory.
#

FALSE=0
TRUE=1

_HAVE_WGET=$FALSE
_HAVE_CURL=$FALSE

which wget >/dev/null && _HAVE_WGET=$TRUE
which curl >/dev/null && _HAVE_CURL=$TRUE

# Using separate values since there could be bugs with
# a specific version.
_DUB_COMMIT="8f1f7e3c05abc2734020727892988051ce25f29e"
_CNI_COMMIT="8f1f7e3c05abc2734020727892988051ce25f29e"
_DIR_COMMIT="8f1f7e3c05abc2734020727892988051ce25f29e"
_TSH_COMMIT="1bfc9295af9ec716bf5b26c70f50b0187d7148bf"

_DUB_RAW_URL="https://osdn.net/users/nemophila/pf/mlib/scm/blobs/$_DUB_COMMIT/dub.sdl?export=raw"
_RDM_RAW_URL="https://osdn.net/users/nemophila/pf/mlib/scm/blobs/$_DUB_COMMIT/README?export=raw"
_LIC_RAW_URL="https://osdn.net/users/nemophila/pf/mlib/scm/blobs/$_DUB_COMMIT/LICENSE?export=raw"

_CNI_RAW_URL="https://osdn.net/users/nemophila/pf/mlib/scm/blobs/$_CNI_COMMIT/source/mlib/cni.d?export=raw"
_DIR_RAW_URL="https://osdn.net/users/nemophila/pf/mlib/scm/blobs/$_DIR_COMMIT/source/mlib/directories.d?export=raw"
_TSH_RAW_URL="https://osdn.net/users/nemophila/pf/mlib/scm/blobs/$_TSH_COMMIT/source/mlib/trash.d?export=raw"

if [ -d mlib ]
then
    rm -rf mlib
fi

if [ $TRUE -eq $_HAVE_WGET ]
then
    # Update by fetching archive with wget
    mkdir -p mlib/source/mlib
    cd mlib
    wget -O "dub.sdl" "$_DUB_RAW_URL" && sleep 2
    wget -O "README" "$_RDM_RAW_URL" && sleep 2
    wget -O "LICENSE" "$_LIC_RAW_URL" && sleep 2
    cd source/mlib
    wget -O "cni.d" "$_CNI_RAW_URL" && sleep 2
    wget -O "directories.d" "$_DIR_RAW_URL" && sleep 2
    wget -O "trash.d" "$_TSH_RAW_URL"
elif [ $TRUE -eq $_HAVE_CURL ]
then
    # Update by fetching archive with curl
    mkdir -p mlib/source/mlib
    cd mlib
    curl -o "dub.sdl" "$_DUB_RAW_URL" && sleep 2
    curl -o "README" "$_RDM_RAW_URL" && sleep 2
    curl -o "LICENSE" "$_LIC_RAW_URL" && sleep 2
    cd source/mlib
    curl -o "cni.d" "$_CNI_RAW_URL" && sleep 2
    curl -o "directories.d" "$_DIR_RAW_URL" && sleep 2
    curl -o "trash.d" "$_TSH_RAW_URL"
fi
