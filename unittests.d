/*
 * Copyright (C) 2022, 2023 dawning.
 *
 * This file is part of medialist-cli.
 *
 * medialist-cli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * medialist-cli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with medialist-cli.  If not, see <https://www.gnu.org/licenses/>.
 */
module unittests;

version(unittest):

import std.conv;
import std.datetime.date;
import std.file;
import std.path;
import std.string;

import std.datetime : Clock;
import std.stdio : File;

import medialist;
import tap;

template Tuple(T...)
{
    alias Tuple = T;
}

shared static this()
{
    import core.runtime;

    /* Don't run the actual unittests :O */
    Runtime.moduleUnitTester = { return true; };
}

void main()
{
    alias tests = Tuple!(__traits(getUnitTests, unittests));

    plan(tests.length);

    foreach(i, test; tests) {
        alias attributes = Tuple!(__traits(getAttributes, tests[i]));

        try {
            test();
            pass(attributes[0]);
        } catch (Throwable t) {
            fail(attributes[0]);
            diag("In test " ~ attributes[0]);
            diag("Caught exception: " ~ t.msg);
        }
    }

    doneTesting();
}

/*
 * Ticket 5b28c4e8174fb4dd
 * After creating a new list and adding a new item to it, calling subsequent modifications
 * (e.g. update and delete) would cause the "new" headers to be removed.
 *
 * This issue occurred when the "start_date", "end_date", and "last_updated" headers were
 * first added, so the previous code assumes the header line was TITLE\tSTATUS\tPROGRESS,
 * while the new code shouldn't assume the position of anything.
 */
@(`The "update" command doesn't remove any headers (5b28c4e8174fb4dd).`)
unittest
{
    enum listName = "unittest-5b28c4e8174fb4dd";
    enum fileName = listName ~ ".tsv";

    MediaList* ml = ml_open_list(listName);
    assert(ml !is null, "ml_open_list returned null");

    scope(exit) {
        ml_send_command(ml, MLCommand.delete_, []);
        ml_free_list(ml);
    }

    MLError ec;

    ml_send_command(ml, MLCommand.add, ["Item 1"], ec);
    assert(MLError.success == ec, "Failed to send add command [Item 1]");

    MediaListHeader[] headers = ml_fetch_headers(ml);
    size_t originalLength = headers.length;

    ml_send_command(ml, MLCommand.update, ["1", "status::COMPLETE"], ec);
    assert(MLError.success == ec, "Failed to update item's status to COMPLETE.");

    MediaListHeader[] secondHeaders = ml_fetch_headers(ml);
    assert(headers == secondHeaders, "Different headers after UPDATE command.");
}

/*
 * Ticket d2ed4a04d0
 * Manually update the "start_date" and "end_date" fields of an item.
 */
@(`Can manually update the "start_date" and "end_date" (d2ed4a04d0).`)
unittest
{
    enum listName = "unittest-d2ed4a04d0";
    enum fileName = listName ~ ".tsv";

    MediaList* ml = ml_open_list(fileName);
    assert(ml !is null, "ml_open_list returned null");

    scope(exit) {
        ml_send_command(ml, MLCommand.delete_, []);
        ml_free_list(ml);
    }

    MLError ec;

    ml_send_command(ml, MLCommand.add, ["Item 1"], ec);
    assert(MLError.success == ec, "Failed to send ADD command [Item 1].");

    MediaListItem item1 = ml_fetch_item(ml, 1, ec);
    assert(MLError.success == ec, "Failed to FETCH id 1.");
    assert(item1.startDate == "", "Non-empty start date for new item.");
    assert(item1.endDate == "", "Non-empty end date for new item.");

    ml_send_command(ml, MLCommand.update, ["1", "start_date::2021-11-25"], ec);
    assert(MLError.success == ec, "Failed to send UPDATE command to change start date.");

    item1 = ml_fetch_item(ml, 1, ec);
    assert(MLError.success == ec, "Failed to FETCH id 1.");
    assert(item1.startDate == "2021-11-25", "Failed to update start date to '2021-11-25'.");
    assert(item1.endDate == "", "Non-empty end date encountered");

    ml_send_command(ml, MLCommand.update, ["1", "end_date::2021-11-25"], ec);
    assert(MLError.success == ec, "Failed to send UPDATE command to change end date.");

    item1 = ml_fetch_item(ml, 1, ec);
    assert(MLError.success == ec, "Failed to FETCH id 1.");
    assert(item1.startDate == "2021-11-25", "Failed to retain start date as '2021-11-25'.");
    assert(item1.endDate == "2021-11-25", "Failed to update end date to '2021-11-25'.");
}

/*
 * Ticket 8c28f8b0aa
 * Update the "last_updated" field automatically. I can't really test this since
 * the value would only change if the "update" command was run on a different day
 * to when the item was added/last updated.
 */
@(`The "last_updated" field is updated automatically (8c28f8b0aa).`)
unittest
{
    import std.datetime.systime : Clock;
    import std.datetime.date : Date;
    import std.format : format;

    enum listName = "unittest-8c28f8b0aa";
    enum fileName = listName ~ ".tsv";

    immutable date = cast(Date)Clock.currTime();

    MediaList* ml = ml_open_list(fileName);
    assert(ml !is null, "ml_open_list returned null.");

    scope(exit) {
        ml_send_command(ml, MLCommand.delete_, []);
        ml_free_list(ml);
    }

    MLError err;

    ml_send_command(ml, MLCommand.add, ["Item 1"], err);
    assert(MLError.success == err, "Failed to send ADD command [Item 1].");

    string currentDate = format!"%d-%02d-%02d"(date.year, date.month, date.day);

    MediaListItem item = ml_fetch_item(ml, 1, err);
    assert(MLError.success == err, "Failed to FETCH item ID 1.");
    assert(item.lastUpdated == currentDate,
        "'last_updated' field does not match the current date.");
}

/*
 * Ticket 4d4545979a
 * Running the 'update' command on a list item that existed before
 * medialist-cli 0.2 doesn't add the "last_updated" cell.
 *
 * Since this test runs on v0.2 and above, we manually create a list
 * to test.
 */
@(`Missing fields are added to items upon update (4d4545979a).`)
unittest
{
    import std.string : split, strip;

    import medialist;

    enum listName = "unittest-4d4545979a";
    enum fileName = listName ~ ".tsv";

    MediaList* ml = ml_open_list(fileName);
    assert(ml !is null, "ml_open_list returned null.");

    scope (exit) {
        ml_send_command(ml, MLCommand.delete_, []);
        ml_free_list(ml);
    }

    File listFile = File(fileName, "a+");
    listFile.writeln("4d4545979a\tUNKNOWN\t??/??"); // id will be 1
    listFile.close();

    MLError err;

    ml_send_command(ml, MLCommand.update, ["1", "progress::-/-"], err);
    assert(MLError.success == err, "Failed to send UPDATE command [1, progress::-/-].");

    listFile = File(fileName);
    string line;
    string[] headers;
    bool readHeader = false;

    while ((line = listFile.readln()) !is null) {
        if (line[0] == '#')
            continue;

        if (false == readHeader) {
            headers = strip(line).split("\t");
            readHeader = true;
            continue;
        }

        /* first non-comment line will be our line */
        line = strip(line);
        break;
    }

    string[] sections = split(line, "\t");
    assert(headers.length == sections.length, "Missing columns were not added.");

    MediaListHeader[] mlHeaders = ml_fetch_headers(ml);

    size_t lu_index;

    foreach(header; mlHeaders) {
        if (header.tsvName == "last_updated") {
            lu_index = header.column;
            break;
        }
    }

    assert("" != sections[lu_index],
        `Missing "last_updated" value after updating pre-0.2 list.`);
}

@(`Create a new list.`)
unittest
{
    const listPath = buildPath(tempDir(), "unittest1.tsv");
    scope(exit) remove(listPath);

    MediaList* list = ml_open_list(listPath);
    scope(exit) ml_free_list(list);

    assert(null !is list, "Memory allocation failed (list is null).");
    assert(true == exists(listPath), "A new file wasn't created for the list.");

    MediaListHeader[] headers = ml_fetch_headers(list);
    assert(null !is headers, "Failed to parse headers");

    foreach(const ref header; headers) {
        assert(null !is header.tsvName, "Failed to find header line in mTSV file");
    }
}

@(`Can add a new item with no progress or status.`)
unittest
{
    const listPath = buildPath(tempDir(), "unittest2.tsv");
    scope(exit) remove(listPath);

    MediaList* list = ml_open_list(listPath);
    assert(null !is list, "Failed to allocate memory for list.");

    scope(exit) ml_free_list(list);

    MLError res;

    ml_send_command(list, MLCommand.add, ["Item 1"], res);
    assert(res == MLError.success, `Failed to send MLCommand.add ["Item 1"].`);

    auto f = File(listPath);
    string line;
    bool readHeader = false;
    string[] sections;

    while ((line = f.readln) !is null) {
        if (line[0] == '#')
            continue;

        if (false == readHeader) {
            readHeader = true;
            continue;
        }

        sections = line.strip().split("\t");
        break;
    }

    Date currentD = cast(Date)Clock.currTime;
    string currentDS = format!"%d-%02d-%02d"(currentD.year,
        currentD.month, currentD.day);

    MediaListHeader[] headers = ml_fetch_headers(list);
    assert(null !is headers, "Failed to fetch headers from list.");
    assert(sections.length == headers.length,
        "Differing amount of headers in unit test file.");

    foreach(const ref header; headers) {
        switch (header.tsvName) {
        case "title":
            assert("Item 1" == sections[header.column],
                `New item with title "Item 1" wasn't saved.`);
            break;
        case "progress":
            assert("-/-" == sections[header.column],
                `New item with unspecified progress wasn't "-/-".`);
            break;
        case "status":
            assert("UNKNOWN" == sections[header.column],
                `New item with unspecified status wasn't "UNKNOWN".`);
            break;
        case "start_date":
            assert("" == sections[header.column],
                `New item does not have blank "start_date" field.`);
            break;
        case "end_date":
            assert("" == sections[header.column],
                `New item does not have blank "end_date" field.`);
            break;
        case "last_updated":
            assert(currentDS == sections[header.column],
                `New item was not "last_updated" with today's date.`);
            break;
        default:
            assert(false, "Unknown header: " ~ header.tsvName);
        }
    }
}

@(`Can add a new item with progress but no status.`)
unittest
{
    const listPath = buildPath(tempDir(), "unittest3.tsv");
    scope(exit) remove(listPath);

    MediaList* list = ml_open_list(listPath);
    assert(null !is list, "Failed to allocate memory for list.");
    scope(exit) ml_free_list(list);

    MLError res;

    ml_send_command(list, MLCommand.add, ["Item 1", "10/-"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add with ["Item 1", "10/-"].`);

    auto f = File(listPath);
    string line;
    bool readHeader = false;
    string[] sections;

    while ((line = f.readln) !is null) {
        if (line[0] == '#')
            continue;

        if (false == readHeader) {
            readHeader = true;
            continue;
        }

        sections = line.strip().split("\t");
    }

    Date currentD = cast(Date)Clock.currTime;
    string currentDS = format!"%d-%02d-%02d"(currentD.year,
        currentD.month, currentD.day);

    MediaListHeader[] headers = ml_fetch_headers(list);
    assert(null !is headers, "Failed to fetch headers from list.");
    assert(sections.length == headers.length,
        "Differing amount of headers in unit test file.");

    foreach(const ref header; headers) {
        switch (header.tsvName) {
        case "title":
            assert("Item 1" == sections[header.column],
                `New item with title "Item 1" wasn't saved.`);
            break;
        case "progress":
            assert("10/-" == sections[header.column],
                `New item with progress "10/-" wasn't saved.`);
            break;
        case "status":
            assert("UNKNOWN" == sections[header.column],
                `New item with unspecified status wasn't "UNKNOWN".`);
            break;
        case "start_date":
            assert("" == sections[header.column],
                `New item does not have blank "start_date" field.`);
            break;
        case "end_date":
            assert("" == sections[header.column],
                `New item does not have blank "end_date" field.`);
            break;
        case "last_updated":
            assert(currentDS == sections[header.column],
                `New item was not "last_updated" with today's date.`);
            break;
        default:
            assert(false, "Unknown header: " ~ header.tsvName);
        }
    }
}

@(`Can add a new item with status but no progress.`)
unittest
{
    const listPath = buildPath(tempDir(), "unittest3.tsv");
    scope(exit) remove(listPath);

    MediaList* list = ml_open_list(listPath);
    assert(null !is list, "Failed to allocate memory for list.");
    scope(exit) ml_free_list(list);

    MLError res;

    ml_send_command(list, MLCommand.add, ["Item 1", null, "COMPLETE"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add with ["Item 1", null, "COMPLETE"].`);

    auto f = File(listPath);
    string line;
    bool readHeader = false;
    string[] sections;

    while ((line = f.readln) !is null) {
        if (line[0] == '#')
            continue;

        if (false == readHeader) {
            readHeader = true;
            continue;
        }

        sections = line.strip().split("\t");
    }

    Date currentD = cast(Date)Clock.currTime;
    string currentDS = format!"%d-%02d-%02d"(currentD.year,
        currentD.month, currentD.day);

    MediaListHeader[] headers = ml_fetch_headers(list);
    assert(null !is headers, "Failed to fetch headers from list.");
    assert(sections.length == headers.length,
        "Differing amount of headers in unit test file.");

    foreach(const ref header; headers) {
        switch (header.tsvName) {
        case "title":
            assert("Item 1" == sections[header.column],
                `New item with title "Item 1" wasn't saved.`);
            break;
        case "progress":
            assert("-/-" == sections[header.column],
                `New item with unspecified progress wasn't "-/-".`);
            break;
        case "status":
            assert("COMPLETE" == sections[header.column],
                `New item with status "COMPLETE" wasn't saved.`);
            break;
        case "start_date":
            assert("" == sections[header.column],
                `New item does not have blank "start_date" field.`);
            break;
        case "end_date":
            assert("" == sections[header.column],
                `New item does not have blank "end_date" field.`);
            break;
        case "last_updated":
            assert(currentDS == sections[header.column],
                `New item was not "last_updated" with today's date.`);
            break;
        default:
            assert(false, "Unknown header: " ~ header.tsvName);
        }
    }
}

@(`Can add a new item with progress and status.`)
unittest
{
    const listPath = buildPath(tempDir(), "unittest3.tsv");
    scope(exit) remove(listPath);

    MediaList* list = ml_open_list(listPath);
    assert(null !is list, "Failed to allocate memory for list.");
    scope(exit) ml_free_list(list);

    MLError res;

    ml_send_command(list, MLCommand.add, ["Item 1", "10/-", "COMPLETE"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add with ["Item 1", "10/-", "COMPLETE"].`);

    auto f = File(listPath);
    string line;
    bool readHeader = false;
    string[] sections;

    while ((line = f.readln) !is null) {
        if (line[0] == '#')
            continue;

        if (false == readHeader) {
            readHeader = true;
            continue;
        }

        sections = line.strip().split("\t");
    }

    Date currentD = cast(Date)Clock.currTime;
    string currentDS = format!"%d-%02d-%02d"(currentD.year,
        currentD.month, currentD.day);

    MediaListHeader[] headers = ml_fetch_headers(list);
    assert(null !is headers, "Failed to fetch headers from list.");
    assert(sections.length == headers.length,
        "Differing amount of headers in unit test file.");

    foreach(const ref header; headers) {
        switch (header.tsvName) {
        case "title":
            assert("Item 1" == sections[header.column],
                `New item with title "Item 1" wasn't saved.`);
            break;
        case "progress":
            assert("10/-" == sections[header.column],
                `New item with progress "10/-" wasn't saved.`);
            break;
        case "status":
            assert("COMPLETE" == sections[header.column],
                `New item with status "COMPLETE" wasn't saved.`);
            break;
        case "start_date":
            assert("" == sections[header.column],
                `New item does not have blank "start_date" field.`);
            break;
        case "end_date":
            assert("" == sections[header.column],
                `New item does not have blank "end_date" field.`);
            break;
        case "last_updated":
            assert(currentDS == sections[header.column],
                `New item was not "last_updated" with today's date.`);
            break;
        default:
            assert(false, "Unknown header: " ~ header.tsvName);
        }
    }
}

@(`Can fetch a singular item by ID (2).`)
unittest
{
    MediaList* list = null;
    list = ml_open_list(buildPath(getcwd(), "unittest_fetchItem2.tsv"));
    assert(null !is list, "Failed to allocate memory for list.");

    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MLError ec;

    ml_send_command(list, MLCommand.add, ["Item 1"], ec);
    assert(MLError.success == ec, `Failed to send MLCommand.add ["Item 1"].`);

    ml_send_command(list, MLCommand.add, ["Item 2", "1/-", "READING"], ec);
    assert(MLError.success == ec,
        `Failed to send MLCommand.add ["Item 2", "1/-", "READING"].`);

    MediaListItem item2 = ml_fetch_item(list, 2, ec);
    assert(MLError.success == ec,
        `Failed to fetch item 2 (ec == ` ~ to!string(ec) ~ `).`);

    assert("Item 2" == item2.title,
        `ml_fetch_item(2).title != "Item 2" (got: ` ~ item2.title ~ `).`);
    assert("1/-" == item2.progress,
        `ml_fetch_item(2).progress != "1/-" (got: ` ~ item2.progress ~ `).`);
    assert("READING" == item2.status,
        `ml_fetch_item(2).status != "READING" (got: ` ~  item2.status ~ `).`);
    assert("" == item2.startDate,
        `ml_fetch_item(2).startDate != "" (got: ` ~ item2.startDate ~ `).`);
    assert("" == item2.endDate,
        `ml_fetch_item(2).endDate != "" (got: ` ~ item2.endDate ~ `).`);
}

/*
 * Emulate pre-0.2 medialist files to confirm that headers can be in any order
 * and that it is case-insensitive.
 */
@(`Check header position variance and case-insensitivity.`)
unittest
{
    enum listName = "checkPre02MLFileHeaderOrder";
    enum fileName = listName ~ ".tsv";

    /* case-insensitive */
    auto listFile = File(fileName, "w+");
    listFile.writeln("TITLE\tPROGRESS\tSTATUS");
    listFile.writeln("Item 1\t??/??\tUNKNOWN");
    listFile.close();

    MediaList *list = ml_open_list(fileName);
    assert(null !is list, "Failed to allocate memory for list.");

    /* shouldn't crash */
    MediaListHeader[] headers = ml_fetch_headers(list);
    assert(3 == headers.length, "ml_fetch_headers IS case-sensitive.");

    /* shouldn't crash */
    MediaListItem[] items = ml_fetch_all(list);

    ml_send_command(list, MLCommand.delete_, []);
    ml_free_list(list);

    listFile = File(fileName, "w+");
    listFile.writeln ("PROGRESS\tTItLE\tstatus");
    listFile.writeln ("??/??\tItem 1\tUNKNOWN");
    listFile.close();

    list = ml_open_list(fileName);
    items = ml_fetch_all(list);

    headers = ml_fetch_headers(list);
    assert(3 == headers.length, "ml_fetch_headers USES hard-coded positions");

    ml_send_command(list, MLCommand.delete_, []);
    ml_free_list(list);
}

@(`Can fetch multiple items by ID (1, 4, and 2).`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    assert(null !is list, "Failed to allocate memory for list.");

    scope (exit) {
        /* delete the entire list and file. */
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MLError res;

    ml_send_command(list, MLCommand.add, ["Item 1", "PLAN-TO-READ", "-/-"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add ["Item 1", "PLAN-TO-READ", "-/-"].`);

    ml_send_command(list, MLCommand.add, ["Item 2", "READING", "10/12"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add ["Item 2", "READING", "10/12".`);

    ml_send_command(list, MLCommand.add, ["Item 3"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add ["Item 3"].`);

    ml_send_command(list, MLCommand.add, ["Item 4", "COMPLETE", "123/123"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add ["Item 4", "COMPLETE", "123/123"].`);

    MediaListItem[] specificItems = ml_fetch_items(list, 1, 4, 2);
    assert(null !is specificItems,
        `ml_fetch_items(list, null, 1, 4, 2) returned null.`);

    assert(3 == specificItems.length,
        `ml_fetch_items(list, null, 1, 4, 2) didn't return 3 items.`);

    assert("Item 1" == specificItems[0].title,
        `specificItems[0].title != "Item 1".`);

    assert("Item 2" == specificItems[1].title,
        `specificItems[1].title != "Item 2".`);

    assert("Item 4" == specificItems[2].title,
        `specificItems[2].title != "Item 4".`);
}

@(`Can update an item's title.`)
unittest
{

    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    assert(null !is list, `Failed to allocate memory for list.`);

    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MLError res;

    ml_send_command(list, MLCommand.add, ["Ietm 1"], res);
    assert(MLError.success == res, `Failed to send MLCommand.add ["Ietm 1"].`);

    ml_send_command(list, MLCommand.update, ["1", "title::Item 1"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.update ["1", "title::Item 1"].`);

    MediaListItem item = ml_fetch_item(list, 1, res);
    assert(MLError.success == res, `Failed to fetch item 1 from list.`);

    assert("Item 1" == item.title,
        `Failed to update title from "Ietm 1" to "Item 1".`);
}

@(`Can update an item's status.`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    assert(null !is list, "Failed to allocate memory for list.");

    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MLError res;

    ml_send_command(list, MLCommand.add, ["Item 1", null, "PLAN-TO-READ"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add ["Item 1", null, "PLAN-TO-READ"].`);

    ml_send_command(list, MLCommand.update, ["1", "status::READING"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.update ["1", "status::READING"].`);

    MediaListItem item = ml_fetch_item(list, 1, res);
    assert(MLError.success == res, `Failed to fetch item 1 from list.`);

    assert("READING" == item.status,
        `Failed to update status from "PLAN-TO-READ" to "READING".`);
}

@(`Can update an item's progress.`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    assert(null !is list, "Failed to allocate memory for list.");

    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MLError res;

    ml_send_command(list, MLCommand.add, ["Item 1"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add ["Item 1"].`);

    ml_send_command(list, MLCommand.update, ["1", "progress::10/10"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.update ["1", "progress::10/10"].`);

    MediaListItem item = ml_fetch_item(list, 1);
    assert(true == item.valid, `Failed to fetch item 1 from list.`);
    assert("10/10" == item.progress,
        `Failed to update progress from "-/-" to "10/10".`);
}

@(`Can update all aspects of an item.`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    assert(null !is list, "Failed to allocate memory for list.");

    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MLError res;

    ml_send_command(list, MLCommand.add, ["Ietm 1"], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.add ["Ietm1"].`);

    ml_send_command(list, MLCommand.update,
        [
            "1",
            "start_date::2021-02-16",
            "end_date::2022-01-20",
            "progress::60/100",
            "status::READING",
            "title::MediaList"
        ], res);
    assert(MLError.success == res,
        `Failed to send MLCommand.update ["1", "start_date::2021-02-16", ` ~
            `"end_date::2022-01-20", "progress::60/100", "status::READING", ` ~
            `"title::MediaList"].`);

    MediaListItem item = ml_fetch_item(list, 1, res);
    assert(MLError.success == res, `Failed to fetch item 1 from list.`);

    assert("MediaList" == item.title,
        `Failed to update item title from "Ietm 1" to "MediaList".`);
    assert("60/100" == item.progress,
        `Failed to update item progress from "-/-" to "60/100".`);
    assert("READING" == item.status,
        `Failed to update item status from "UNKNOWN" to "READING".`);
    assert("2021-02-16" == item.startDate,
        `Failed to update item start date from "" to "2021-02-16".`);
    assert("2022-01-20" == item.endDate,
        `Failed to update item end date from "" to "2022-01-20".`);

    Date date = cast(Date)Clock.currTime;
    string currentDate = format!"%d-%02d-%02d"(date.year, date.month, date.day);
    assert(currentDate == item.lastUpdated,
        `Failed to update item's last_update when sending MLCommand.update.`);
}

@(`Automatically update start/end date`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    assert(null !is list, "Failed to allocate new list.");
    
    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MLError res;
    
    ml_send_command(list, MLCommand.add, ["Item 1", null, "PLAN-TO-READ"], res);
    assert(MLError.success == res, `Failed to send add command ["Item 1", null, "PLAN-TO-READ"]`);
    
    MediaListItem item1 = ml_fetch_item(list, 1, res);
    assert(MLError.success == res, `Failed to fetch item 1`);
    
    assert("" == item1.startDate, "New item with no provided start date is not blank");
    assert("" == item1.endDate, "New item with no provided end date is not blank");
    
    ml_send_command(list, MLCommand.update, ["1", "status::READING"], res);
    assert(MLError.success == res, `Failed to send update command [1, status::READING]`);
    
    item1 = ml_fetch_item(list, 1, res);
    assert(MLError.success == res, `Failed to fetch item 1`);
    
    assert("" != item1.startDate, "PLAN-TO-READ to READING does not change empty start date.");
    assert("" == item1.endDate, "PLAN-TO-READ to READING changed empty end date.");
    
    string startDate = item1.startDate;
    
    ml_send_command(list, MLCommand.update, ["1", "status::COMPLETE"], res);
    assert(MLError.success == res, `Failed to send update command [1, status::COMPLETE]`);
    
    item1 = ml_fetch_item(list, 1, res);
    assert(MLError.success == res, `Failed to fetch item 1`);
    
    assert(startDate == item1.startDate, `READING to COMPLETE changed start date.`);
    assert("" != item1.endDate, `READING to COMPLETE did not change empty end date.`);
}

@(`Can delete a single item from a list`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MediaListItem item1;
    MLError error;

    ml_send_command(list, MLCommand.add, ["Item 1"], error);
    assert(MLError.success == error, "Failed to add new item to list.");

    item1 = ml_fetch_item(list, 1, error);
    assert(MLError.success == error, "Failed to retrieve index 1 with list containing 1 item.");

    ml_send_command(list, MLCommand.delete_, ["1"], error);
    assert(MLError.success == error, "Failed to delete index 1 with list containing 1 item.");

    assert(ml_fetch_all(list).length == 0, "List doesn't have 0 length when it should contain no elements.");

    item1 = ml_fetch_item(list, 1, error);
    assert(MLError.itemNotFound == error, "Found an item at index 1 with list containing 0 items.");
}

@(`Can delete multiple items from a list`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MediaListItem[] items;
    MLError error;

    foreach(i; 1..4) {
        ml_send_command(list, MLCommand.add, ["Item " ~ to!string(i)], error);
        assert(MLError.success == error, "Failed to add new items to new list.");
    }

    items = ml_fetch_items(list, error, [1, 3]);
    assert(items.length == 2, "Returned items array doesn't contain two items");
    assert("Item 1" == items[0].title, "First (idx:1) item title doesn't match.");
    assert("Item 3" == items[1].title, "Second(idx:3) item title doesn't match.");

    ml_send_command(list, MLCommand.delete_, ["1", "3"], error);
    assert(MLError.success == error, "Failed to delete item index 1 and 3");

    items = ml_fetch_items(list, [1, 3]);
    assert(items.length == 1, "Returned item array doesn't contain one item");
    assert(items[0].title == "Item 2", "First (idx:1 was idx:2) item title doesn't match.");
}


/*
 * Ticket d247886e46
 *
 * Since all the commands expect a 1-based index (with medialist
 * automatically adjusting) passing an index of 0 should either set the
 * error to MLError.itemNotFound or throw an MLException -- depending
 * on whether it's the nothrow variant.
 *
 * Basically, there is an error when reading each line where we don't
 * check if we've past the header line.
 */
@(`ml_fetch_item doesn't work with index 0 (d247886e46)`)
unittest
{
    enum listName = __FUNCTION__;
    enum fileName = listName ~ ".tsv";

    MediaList* list = ml_open_list(fileName);
    scope(exit) {
        ml_send_command(list, MLCommand.delete_, []);
        ml_free_list(list);
    }

    MediaListItem item;
    MLError error;

    item = ml_fetch_item(list, 0, error);
    assert(MLError.success != error,
        "ml_fetch_item with index 0 (0 items) returned \"successfully\" when expected error.");

    ml_send_command(list, MLCommand.add, ["Item 1"]);
    item = ml_fetch_item(list, 0, error);
    assert(MLError.success != error,
        "ml_fetch_item with index 0 (1 item) returned \"successfully\" when expected error.");
}
