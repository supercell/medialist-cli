/*
 * Copyright (C) 2021-2023 dawning.
 *
 * This file is part of medialist-cli.
 *
 * medialist-cli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * medialist-cli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with medialist-cli.  If not, see <https://www.gnu.org/licenses/>.
 */
import core.stdc.stdlib : EXIT_FAILURE, EXIT_SUCCESS;

import std.algorithm.searching;
import std.algorithm.sorting;
import std.conv;
import std.experimental.logger;
import std.file;
import std.path;
import std.stdio;
import std.string;

import std.json : JSONValue;

import mlib.cni;
import mlib.directories;
import mlib.trash : trash;

import medialist;
import util : expandEnvironmentVariables;

enum MediaListCLIVersion = "0.4";

@trusted void display_usage(string name)
{
    writefln("Usage: %s [options] <command> [command_options...]

Options:
  -h, --help      display this help and exit
  -v, --version   output version information and exit

Commands:
  add <listname> <title> [--status STATUS] [--progress PROGRESS]
      Add a new item to 'listname' with called 'title'.  You can optionally
      specify the current status of the item (e.g. reading), and your
      current progress through the item (e.g. 12/12).  What these values
      mean is up to you.

  create <listname>
     Create a new list called 'listname'.  If a list with the same name
     already exists, an error will be displayed.

  delete <listname> [<id>...]
     Delete the list 'listname'.  If one or more item IDs are provided,
     then those items are deleted from the list and the list will remain.

  update <listname> <id> [--title TITLE] [--status STATUS] [--progress PROGRESS]
                         [--start-date DATE] [--end-date DATE]
     Update the item 'id' on the list 'listname' with the (optional) new
     title, status, or progress.  More than one option can be specified.

     The start and end DATE format is YYYY-MM-DD.

  show <listname> [-n|--numbered] [--show-headers]
     Show the list 'listname'.  If the '--numbered' option is used then
     the ID for each item is printed at the start of each line.

     The --show-headers option will print the headers from the mTSV file.

  export <listname> [-t|--type TYPE]
     Export the list 'listname'.  The default TYPE is 'json', but it's
     also possible to export the list as a HTML table with 'html'.

  import <list.json> [--update] [--overwrite] [--overwrite-list]
     Import a list from 'list.json'.  The structure of the JSON is expected
     to match the structure of the JSON from the 'export' command.  This
     will create a list matching the \"name\" field located where specified
     in your configuration file.

     If a list already exists with the same name, then, by default, it will
     append any new items to the existing list.  If list.json contains an
     item with the same name as one that already exists, then it is skipped.

     You can choose to update (--update) or overwrite (--overwrite) existing
     items.  The former will only change an existing item if the last_updated
     field is newer in list.json.  Overwrite will change the existing item
     regardless of last_updated.

     If a list already exists with the same name as that found in list.json,
     you can choose to overwrite ALL the existing items with --overwrite-list.
     THIS WILL REMOVE ALL ITEMS FROM THE PREVIOUSLY EXISTING LIST WHICH ARE
     NOT FOUND IN list.json!

Report bugs to <stigma+mlcli@disroot.org>.
View current bugs: <https://rocketgit.com/user/dawning/medialist-cli/bug>
medialist-cli web page: <https://yume-neru.neocities.org/p/medialist.html>",
        name);
}

/**
 * Increase the number of TAB (`\t`) characters present in each line
 * from *list*.
 * 
 * Params:
 *   list = The list in which to increase the number of TABs
 *   totalNumber = The total number of TAB characters for each line
 * 
 * This will read through each line of *list.filePath* and increase
 * the number of TAB characters to the *totalNumber*.  If there are
 * already *totalNumber* or more TAB characters present in a line,
 * then it is left untouched.
 */
private void increaseLineSections(MediaList* list, size_t totalNumber)
{
    import std.algorithm.mutation : rstrip = stripRight;

    File listFile = File(list.filePath);
    list.isOpen = true;
    scope(exit) list.isOpen = false;
    
    File newFile = File(list.filePath ~ ".tmp", "w+");
    
    bool pastHeader = false;
    foreach(string line; listFile.byLineCopy) {
        if (line[0] == '#') {
            newFile.writeln(line);
            continue;
        }
        
        if (false == pastHeader) {
            pastHeader = true;
            newFile.writeln(line);
            continue;
        }

        string[] sections = line.split("\t");
        if (sections.length < totalNumber) {
            string _line = line.rstrip('\n');

            foreach(i; 0..(totalNumber - sections.length)) {
                _line ~= "\t";
            }
            newFile.writeln(_line);
        } else {
            newFile.writeln(line);
        }
    }
    listFile.close();
    list.isOpen = false;
    newFile.close();
    
    copy(list.filePath, list.filePath ~ ".orig");
    rename(list.filePath ~ ".tmp", list.filePath);
}


private bool showList(MediaList* list, string[] args)
{
    import core.exception : RangeError;

    bool numbered = false;
    bool showHeaders = false;

    trace("Command: show");

    foreach(ref arg; args) {
        if ("-n" == arg || "--numbered" == arg)
            numbered = true;

        if ("--show-headers" == arg)
            showHeaders = true;
    }

    if (true == showHeaders) {
        MediaListHeader[] headers = ml_fetch_headers(list);
        string title;
        string progress;
        string status;

        foreach(size_t idx, const ref header; headers) {
            switch(header.tsvName.toLower) {
            case "title":
                title = (header.humanFriendlyName == "")
                        ? header.tsvName
                        : header.humanFriendlyName;
                break;
            case "progress":
                progress = (header.humanFriendlyName is null)
                        ? header.tsvName
                        : header.humanFriendlyName;
                break;
            case "status":
                status = (header.humanFriendlyName is null)
                        ? header.tsvName
                        : header.humanFriendlyName;
                break;
            default:
                break;
            }
        }

        writefln("%s => %s => %s", title, status, progress);
        /* 8 = " => " * 2 */
        size_t lineLength = title.length + status.length + progress.length + 8;
        for (size_t i = 0; i < lineLength; i++) {
            write("=");
        }
        write("\n");
        stdout.flush();
    }

    MediaListItem[] items;
    
    try {
        items = ml_fetch_all(list);
    } catch (RangeError re) {
        // It is possible this was caused because version prior to 0.2.0
        // only required three headers and didn't have the additional
        // trailing tabs required for "end_date", "start_date",
        // and "last_updated".
        
        MediaListHeader[] headers = ml_fetch_headers(list);
        
        // Writing to STDERR should be fine, since the output is written
        // to STDOUT.
        stderr.writeln("WARNING: Encountered an error retrieving items.");
        stderr.writeln("         Will attempt to resolve future occurrences.");
        increaseLineSections(list, headers.length);
        
        // If it happens again, then it must be some other issue.
        items = ml_fetch_all(list);
    }
    if (null is items) {
        stderr.writeln("Failed to retrieve items from list (returned null).");
        return false;
    }

    foreach(size_t idx, const ref item; items) {
        if (true == numbered)
            writef("%d: ", idx + 1);

        writefln("%s => %s => %s", item.title, item.status, item.progress);
    }

    return true;
}

private bool addToList(MediaList* list, string[] args)
{
    trace("Command: add");

    if (args.length == 0)
        return false;

    string title = args[0];
    string status = "UNKNOWN";
    string progress = "-/-";

    for (size_t i = 1; (i + 1) < args.length; i++) {
        string arg = args[i];
        if (arg == "-s" || arg == "--status") {
            status = args[i + 1];
            i += 1;
            continue;
        }

        if (arg == "-p" || arg == "--progress") {
            progress = args[i + 1];
            i += 1;
            continue;
        }
    }

    MLError error;
    ml_send_command(list, MLCommand.add, [title, progress, status], error);

    if (MLError.success != error) {
        stderr.writefln("ERROR: Failed to add %s to %s.", title, list.listName);
        stderr.writeln("ISSUE: List is already open for editing.");
        return false;
    }

    writefln("Successfully added %s to %s (%s, %s).", title, list.listName,
        status, progress);
    return true;
}

private bool updateListItem(MediaList* list, string[] args)
{
    string[] commandArgs;
    size_t id;

    trace("Command: update");

    /*
     * At least: [id, --option, option_value]
     */
    if (3 > args.length)
        return false;

    commandArgs ~= args[0]; // id

    try {
        id = to!size_t(args[0]);
    } catch (ConvException e) {
        stderr.writefln("ERROR: Failed to update the %s list.", list.listName);
        stderr.writefln("ISSUE: Invalid ID '%s'. It must be a positive number.",
            args[0]);
        return false;
    }

    for (size_t i = 1; (i + 1) < args.length; i += 1) {
        string arg = args[i];

        switch (arg) {
        case "-t":
        case "--title":
            commandArgs ~= "title::" ~ args[i + 1];
            i += 1;
            break;
        case "-p":
        case "--progress":
            commandArgs ~= "progress::" ~ args[i + 1];
            i += 1;
            break;
        case "-s":
        case "--status":
            commandArgs ~= "status::" ~ args[i + 1];
            i += 1;
            break;
        case "--start-date":
            commandArgs ~= "start_date::" ~ args[i + 1];
            i += 1;
            break;
        case "--end-date":
            commandArgs ~= "end_date::" ~ args[i + 1];
            i += 1;
            break;
        default:
            break;
        }
    }

    MLError error;
    ml_send_command(list, MLCommand.update, commandArgs, error);

    if (MLError.success != error) {
        /* This can only be because the list is open. */
        stderr.writefln("ERROR: Failed to update list %s.", list.listName);
        stderr.writeln("ISSUE: List is already open for editing.");
        return false;
    }

    MediaListItem item = ml_fetch_item(list, id, error);

    if (MLError.success != error) {
        switch (error) {
        case MLError.itemNotFound:
            /* id doesn't exist. no item was updated. */
            stderr.writefln("ERROR: Failed to update list %s.", list.listName);
            stderr.writefln("ISSUE: No item with ID %d.", id);
            return false;
        case MLError.fileAlreadyOpen:
            stderr.writefln("ERROR: Failed to update list %s.", list.listName);
            stderr.writeln("ISSUE: List is already open for editing.");
            return false;
        default:
            break;
        }
    }

    writefln("Successfully updated %s from %s (%s, %s).", item.title,
        list.listName, item.status, item.progress);

    return true;
}

private bool deleteFromList(MediaList* list, bool shouldVerifyDelete, string[] args)
{
    trace("Command: delete");

    if (args.length == 0)
    {
        if (shouldVerifyDelete)
        {
            writef("Are you sure you want to delete the %s list? (yes/[no]) ", list.listName);
            string res = readln().strip().toLower();

            if ("y" != res && "yes" != res)
            {
                writefln("Cancelled: No longer deleting the %s list.", list.listName);
                return true;
            }
        }

        trash(list.filePath);
        writefln("Successfully deleted the %s list.", list.listName);
        return true;
    }

    size_t[] ids = new size_t[args.length];

    /* Keep the id separate from the loop so we don't delete the list. */
    size_t id;
    foreach (const ref string arg; args)
    {
        try
        {
            ids[id] = to!size_t(arg);
            id += 1;
        }
        catch (ConvException ce)
        {
            stderr.writefln("ERROR: Failed to delete %s from %s.", arg, list.listName);
            stderr.writefln("ISSUE: Couldn't convert %s to an unsigned number.", arg);
            stderr.writeln (" NOTE: No items have been removed from the list.");
            return false;
        }
    }

    /* Sort so the order matches what ml_fetch_items will return */
    ids.sort();

    MLError err;
    MediaListItem[] items = ml_fetch_items(list, err, ids);
    if (MLError.success != err)
    {
        stderr.writefln("ERROR: Failed to fetch items from %s.", list.listName);
        stderr.writefln("ISSUE: MLError.%s", err);
        stderr.writeln (" NOTE: No items have been removed from the list.");
        return false;
    }

    if (shouldVerifyDelete)
    {
        /*
         * Both "ids" and "items" are sorted. "args" is not sorted.
         * Since the order can be different, we convert each id to a string and
         * add that to another array that is sent.
         */
        string[] cmdArgs;

        size_t idx = 0;
        foreach (ref item; items)
        {
            writef("Are you sure you want to delete %s (yes/[no]) ", item.title);
            string res = readln().strip().toLower();

            if ("yes" == res)
            {
                writefln("  Will delete %s.", item.title);
                cmdArgs ~= to!string(ids[idx]);
                item.valid = false;
            }
            else
            {
                writefln("  Will not delete %s.", item.title);
            }
            idx += 1;
        }
        ml_send_command(list, MLCommand.delete_, cmdArgs, err);
    }
    else
    {
        ml_send_command(list, MLCommand.delete_, args, err);
    }

    if (MLError.success == err)
    {
        foreach (const ref MediaListItem item; items)
        {
            if (false == item.valid)
                writefln("Successfully deleted %s from %s.", item.title,
                    list.listName);
        }
        return true;
    }

    return false;
}


/*
 * Structure of JSON response:
 * 
 * Any missing values will be represented by the Empty String ""
 * 
 * {
 *    "list": <string> // list name
 *    "items": [
 *         {
 *                   "title": <string> // item Title
 *                  "status": <string> // item Status
 *                "progress": <string> // item Progress
 *               "startDate": <string> // item start_date
 *                 "endDate": <string> // item end_date
 *             "lastUpdated": <string> // item last_updated
 *         }, <...>
 *    ]
 * }
 * 
 * The individual list items will also contain any additional headers
 * that are present in the mTSV file.  Their values will be represented
 * as strings.
 */
private bool exportListJSON(MediaList* list)
{
    import core.exception : RangeError;
    import std.json : JSONValue;

    MLError error;

    trace("export: JSON");
    
    JSONValue root = [ "name": list.listName ];

    MediaListItem[] items = ml_fetch_all(list, error);

    // Shortcoming of medialist v0.3.0-pre: MLError is only an enum
    // and doesn't really support handling "unspecifiedError".
    if (MLError.unspecifiedError == error) {
        // This could be anything, really. So we'll try again.

        MediaListHeader[] headers = ml_fetch_headers(list, error);

        // Writing to STDERR should be find, since the output is written
        // to STDOUT.
        stderr.writeln("WARNING: Encountered an error while retrieving items.");
        stderr.writeln("         Attempting to resolve for future occurrences.");
        increaseLineSections(list, headers.length);

        items = ml_fetch_all(list, error);
    }
    
    if (MLError.fileAlreadyOpen == error) {
        stderr.writefln("ERROR: Failed to fetch list items from '%s'.",
            list.listName);
        stderr.writeln ("ISSUE: List is already open for editing.");
        return false;
    }
    
    JSONValue[] jsonItems;
    
    foreach(item; items) {
        JSONValue jsonItem = [
            "title": item.title,
            "progress": item.progress,
            "status": item.status,
            "lastUpdated": item.lastUpdated,
            "startDate": item.startDate,
            "endDate": item.endDate
        ];
        jsonItems ~= jsonItem;
    }
    
    root["items"] = jsonItems;
    
    writeln(root.toPrettyString());
    
    return true;
}

/**
 * Export the MediaList *list* to a simple, un-styled HTML table.
 */
private bool exportListHTML(MediaList* list)
{
    import core.exception : RangeError;
    import std.outbuffer;

    trace("export: HTML");
    
    auto buffer = new OutBuffer();
    scope(exit) buffer.clear();
    
    MediaListItem[] items;
    
    try {
        items = ml_fetch_all(list);
    } catch (RangeError re) {
        // It is possible this was caused because version prior to 0.2.0
        // only required three headers and didn't have the additional
        // trailing tabs required for "end_date", "start_date",
        // and "last_updated".
        
        MediaListHeader[] headers = ml_fetch_headers(list);
        
        // Writing to STDERR should be fine, since the output is written
        // to STDOUT.
        stderr.writeln("WARNING: Encountered an error retrieving items.");
        stderr.writeln("         Will attempt to resolve future occurrences.");
        increaseLineSections(list, headers.length);
        
        // If it happens again, then it must be some other issue.
        items = ml_fetch_all(list);
    }
    
    buffer.write("<table>\n");
    buffer.write("    <thead>\n");
    buffer.write("        <tr>\n");
    buffer.write("            <th>Title</th>\n");
    buffer.write("            <th>Status</th>\n");
    buffer.write("            <th>Progress</th>\n");
    buffer.write("            <th>Start Date</th>\n");
    buffer.write("            <th>End Date</th>\n");
    buffer.write("            <th>Last Updated</th>\n");
    buffer.write("        </tr>\n");
    buffer.write("    </thead>\n");
    buffer.write("    <tbody>\n");
    
    foreach(item; items) {
    buffer.write ("        <tr>\n");
    buffer.writef("            <td>%s</td>\n", item.title);
    buffer.writef("            <td>%s</td>\n", item.status);
    buffer.writef("            <td>%s</td>\n", item.progress);
    buffer.writef("            <td>%s</td>\n", item.startDate);
    buffer.writef("            <td>%s</td>\n", item.endDate);
    buffer.writef("            <td>%s</td>\n", item.lastUpdated);
    buffer.write ("        </tr>\n");
    }

    buffer.write("    </tbody>\n");
    buffer.write("</table>");
    
    writeln(buffer.toString());

    return true;
}

///
/// Export a list.
///
/// For more information about the format of the different outputs,
/// see their respective functions: exportListJSON, exportListHTML.
///
private bool exportList(MediaList* list, string[] args)
{
    enum ExportType { json, html }
    
    ExportType exportType;

    trace("Command: export");
    
    if (0 == args.length) {
        exportType = ExportType.json;
    } else if (2 > args.length) {
        stderr.writefln("ERROR: Failed to export list '%s'", list.listName);
        stderr.writeln ("ISSUE: No export type specified. " ~
            "Must be either 'json' or 'html'.");
        return false;
    } else {
        if (args[1] == "json") {
            exportType = ExportType.json;
        } else if (args[1] == "html") {
            exportType = ExportType.html;
        } else {
            stderr.writefln("ERROR: Failed to export list '%s'", list.listName);
            stderr.writefln("ISSUE: Invalid export type '%s'. " ~
                "Must be either 'json' or 'html'.", args[1]);
            return false;
        }
    }
    
    if (exportType == ExportType.json)
        return exportListJSON(list);

    return exportListHTML(list);
}

///
/// Create a MediaListItem from a JSON object.
///
/// If one of the member fields are not found in *json*, then a default value
/// is used.
///
/// |Field        |Default     |
/// |-------------|------------|
/// |Title        |No Title    |
/// |Status       |Unknown     |
/// |Progress     |-/-         |
/// |Start Date   |(no value)  |
/// |End Date     |(no value)  |
/// |Last Updated |Today's Date|
///
/// Last Updated will use the current date (`YYYY-MM-DD`) if there is no
/// previous value.
///
/// Params:
///   json = The parsed JSONValue containing a MediaListItem definition.
///
/// Returns: A serialized MediaListItem structure containing the information
///          from *json*.
///
private MediaListItem mediaListItemFromJSON(in ref JSONValue json) {
    MediaListItem item;

    if ("title" in json)
        item.title = json["title"].str();
    else
        item.title = "No Title";
    
    if ("status" in json)
        item.status = json["status"].str();
    else
        item.status = "Unknown";

    if ("progress" in json)
        item.progress = json["progress"].str();
    else
        item.progress = "-/-";
    
    if ("startDate" in json)
        item.startDate = json["startDate"].str();
    else
        item.startDate = "";
    
    if ("endDate" in json)
        item.endDate = json["endDate"].str();
    else
        item.endDate = "";
    
    if ("lastUpdated" in json)
        item.lastUpdated = json["lastUpdated"].str();
    else {
        import std.datetime : Date, Clock;
        Date date = cast(Date)Clock.currTime();
        item.lastUpdated = format!"%d-%02d-%02d"(date.year, date.month, date.day);
    }

    item.valid = true;
    return item;
}

///
/// A structure representing a JSON list import.
///
/// See exportListJSON for an overview of the JSON format.
///
private struct JSONImport {
    string name;
    MediaListItem[] items;

    static JSONImport fromJSON(in ref JSONValue json) {
        JSONImport ret;

        if ("name" !in json) {
            throw new Exception("No \"name\" field in imported JSON");
        }

        if ("items" !in json) {
            throw new Exception("No \"items\" field in the imported JSON");
        }

        ret.name = json["name"].str();
        foreach(ref jsonItem; json["items"].array) {
            auto item = mediaListItemFromJSON(jsonItem);
            ret.items ~= item;
        }

        return ret;
    }
}

///
/// Update all items from *imported* that can be found in *listPath*.
///
/// Params:
///   imported = The list of items imported from the JSON file.
///   listPath = The path to the MediaList file (mTSV file).
///   overwrite = Should any items that exist in both *imported* and *listPath* be overwritten
///               by the values in *imported*, regardless of which is newer.
///
/// Returns: `true` if the update was successfull. Otherwise, false.
///
private bool updateFileItems(MediaListItem[] imported, string listPath, bool overwrite) {
    import std.algorithm.iteration : filter;

    MediaList* list = ml_open_list(listPath);
    scope(exit) ml_free_list(list);

    MediaListItem[] existing;
    MediaListItem[size_t] toUpdate;
    MediaListItem[] newItems;
    MLError error;
    
    existing = ml_fetch_all(list);

    foreach(item; imported) {
        size_t pointer = countUntil!(e => e.title == item.title)(existing);

        // item was not found: it is new
        if (-1 == pointer) {
            newItems ~= item;
            continue;
        }

        // item was found: overwrite regardless.
        if (overwrite) {
            toUpdate[pointer] = item;
            continue;
        }

        // item was found: is it an update?
        if (item > existing[pointer]) {
            toUpdate[pointer] = item;
            continue;
        }

        // item was found, but is older so ignore it.
    }

    foreach(index, item; toUpdate) {
        string[] commandArguments = [
            to!string(index + 1), // MediaList requires a 1-based index (comes from "show" command)
            "title::" ~ item.title,
            "progress::" ~ item.progress,
            "status::" ~ item.status,
            "start_date::" ~ item.startDate,
            "end_date::" ~ item.endDate
        ];
        ml_send_command(list, MLCommand.update, commandArguments, error);

        if (MLError.invalidArgs == error) {
            logf("import: failed %sItems", overwrite ? "overwrite" : "update");
            logf("        existing item: title=>%s, progress=%s, status=>%s, sd=>%s, ed=>%s",
                item.title, item.progress, item.status, item.startDate, item.endDate);
            stderr.writefln("Failed to update to new '%s'", item.title);
            continue;
        } else if (MLError.success == error) {
            writefln("Successfully imported (and %s) %s.",
                overwrite ? "overwrote" : "updated",
                item.title);
        }
    }

    foreach(index, item; newItems) {
        ml_send_command(list, MLCommand.add, [
            item.title,
            item.progress,
            item.status
        ], error);

        if (MLError.invalidArgs == error) {
            stderr.writefln("Failed to add new '%s'", item.title);
            continue;
        }

        ml_send_command(list, MLCommand.update,[
            to!string(existing.length + index + 1),
            "start_date::" ~ item.startDate,
            "end_date::" ~ item.endDate
        ], error);

        if (MLError.invalidArgs == error) {
            stderr.writefln("Failed to finish adding '%s'", item.title);
            continue;
        } else if (MLError.success == error) {
            writefln("Successfully imported (and added) %s.", item.title);
        }
    }

    return true;
}

///
/// Import a list defined in a JSON file located in *args*
///
/// Params:
///  data_dir = The directory where all .tsv files are stored.
///  args = An array containing all the CLI arguments after "import"
///
/// Returns: `true` if we imported successfully, `false` otherwise.
///
private bool importList(string data_dir, string[] args) {
    import std.json : JSONException, parseJSON;

    bool shouldUpdateItems = false;
    bool shouldOverwriteList = false;
    bool shouldOverwriteItems = false;
    string jsonFilePath;
    string rawJson;
    string mtsvFilePath;
    JSONValue parsedJson;
    JSONImport jsonImport;
    File mtsvFile;
    MediaListItem[] itemsToWrite;

    // No file provided
    if (args.length == 0) {
        stderr.writeln("Failed to import JSON: No file provided.");
        return false;
    }

    foreach (arg; args)
    {
        switch(arg) {
            case "-h":
            case "-help":
            case "--help":
            case "help":
                writeln("Usage: medialist-cli import list.json [--update] [--overwrite] [--overwrite-list]");
                writeln("\nIf the list doesn't exist, then a new list is created and filled with " ~
                    "the contents of list.json.");
                writeln("\nIf a list DOES already exist, by default this will append any new items " ~
                    "from list.json to the existing list.");
                writeln("\n--update: This will update items which appear in both lists with the one in list.json, " ~
                    "if it is newer.");
                writeln("\n--overwrite: This will overwrite items which appear in both lists with the one in " ~
                    "list.json, regardless of which is newer.");
                writeln("\n--overwrite-list: This will remove all items from the existing list which are not " ~
                    "found in list.json. It will then import and overwrite all the items found in list.json.");
                return true;
            case "--update":
                shouldUpdateItems = true;
                break;
            case "--overwrite":
                shouldOverwriteItems = true;
                break;
            case "--overwrite-list":
                shouldOverwriteList = true;
                break;
            default:
                if (jsonFilePath == "") {
                    jsonFilePath = arg;
                } else {
                    stderr.writefln("Multiple file names detected:\n %s\n %s",
                        jsonFilePath, arg);
                    stderr.writeln("Please specify one JSON file to import.");
                    return false;
                }
                break;
        }
    }

    if (shouldUpdateItems && shouldOverwriteItems) {
        stderr.writeln("Both --update and --overwrite were provided.");
        stderr.writeln("Do you want to update or overwrite existing items? ([update]/overwrite: ");
        string choice = readln().strip().toLower();
        switch (choice) {
        case "":
        case "update":
            shouldOverwriteItems = false;
            break;
        case "overwrite":
            shouldUpdateItems = false;
            break;
        default:
            stderr.writefln("Invalid choice: %s. Exiting", choice);
            return false;
        }
    }

    if (shouldOverwriteItems && shouldOverwriteList) {
        stderr.writeln("Both --overwrite and --overwrite-list were provided.");
        stderr.write("Do you want to overwrite existing items or the whole list? ([items]/list): ");
        string choice = readln().strip().toLower();
        switch (choice) {
        case "":
        case "items":
            shouldOverwriteList = false;
            break;
        case "list":
            shouldOverwriteItems = false;
            break;
        default:
            stderr.writefln("Invalid choide: %s. Exiting.", choice);
            return false;
        }
    }

    logf("import: updateItems?(%s) overwriteItems?(%s) overwriteList?(%s)",
        shouldUpdateItems ? "true" : "false", shouldOverwriteItems ? "true" : "false",
        shouldOverwriteList ? "true" : "false");

    try {
        rawJson = readText(jsonFilePath);
        parsedJson = parseJSON(rawJson);
        jsonImport = JSONImport.fromJSON(parsedJson);
    } catch (FileException fe) {
        stderr.writefln("Unable to open %s for reading:", jsonFilePath);
        stderr.writefln("  %s", fe.msg);
        return false;
    } catch (JSONException je) {
        stderr.writeln("Unable to parse JSON:");
        stderr.writefln("  %s", je.msg);
        return false;
    } catch (Exception e) {
        stderr.writeln("Error occurred when importing the list:");
        stderr.writefln("  %s", e.msg);
        return false;
    }

    mtsvFilePath = buildPath(data_dir, jsonImport.name ~ ".tsv");

    if (false == exists(mtsvFilePath)) {
        logf("import: No list (%s) exists, creating a new one.", jsonImport.name);
        // Open a list so we have the placeholder content
        // that you would have with medialist-cli --create
        MediaList* list = ml_open_list(mtsvFilePath);
        ml_free_list(list);

        mtsvFile = File(mtsvFilePath, "a+");
        itemsToWrite = jsonImport.items;
    } else if (shouldOverwriteList) {
        logf("import: Overwriting list %s", jsonImport.name);
        trash(mtsvFilePath);

        MediaList* list = ml_open_list(mtsvFilePath);
        ml_free_list(list);

        mtsvFile = File(mtsvFilePath, "a+");
        itemsToWrite = jsonImport.items;
    } else if (shouldUpdateItems || shouldOverwriteItems) {
        logf("import: %s items for list %s", shouldUpdateItems ? "updating" : "overwriting",
            jsonImport.name);
        return updateFileItems(jsonImport.items, mtsvFilePath, shouldOverwriteItems);
    } else {
        // File exists, and we want to append. Only unique items though
        import std.algorithm.iteration : filter;
        import std.array : array;

        log("import: Importing an already existing list. Appending unique items.");

        MediaList* list = ml_open_list(mtsvFilePath);
        MediaListItem[] existing = ml_fetch_all(list);
        ml_free_list(list);

        mtsvFile = File(mtsvFilePath, "a+");

        itemsToWrite = jsonImport.items.filter!(i => !canFind(existing, i)).array;
    }
    assert(mtsvFile.isOpen());

    foreach(item; itemsToWrite) {
        mtsvFile.writeln(item.tsvRepresentation());
    }

    return true;
}

///
/// Setup the logger for medialist-cli.
///
void setupLogger(in ref ProjectDirectories projectDirs)
{
    string logDirPath = projectDirs.cacheDir;

    if (false == exists(logDirPath)) {
        mkdirRecurse(logDirPath);
    }

    string logFilePath = buildPath(logDirPath, "ml-cli.log");

    // Remove a log file that is over a month old.
    if (true == exists(logFilePath)) {
        import std.datetime : Clock, SysTime;
        SysTime currentTime = Clock.currTime;
        SysTime lastAccessed;
        SysTime lastModified;
        getTimes(logFilePath, lastAccessed, lastModified);

        if (currentTime.diffMonths(lastModified) > 1) {
            trash(logFilePath);
        }
    }

    stdThreadLocalLog = new FileLogger(logFilePath);
}

int main(string[] args) {
    ProjectDirectories projectDirs = getProjectDirectories(null, "YumeNeru Software", "medialist");
    string dataDirectory = projectDirs.dataDir; // default directory.

    string command = "help";

version(appimage) {
    string program_name = "medialist-cli";
} else {
    string program_name = args[0];
}

    setupLogger(projectDirs);

    bool success = true;
    /* Default behaviour since 0.3 */
    bool shouldVerifyDelete = true;

    /* --help, --version */
    if (args.length < 2) {
        display_usage(program_name);
        return EXIT_FAILURE;
    }

    switch(args[1]) {
    case "-h":
    case "--help":
        display_usage(program_name);
        return EXIT_SUCCESS;
    case "-v":
    case "--version":
        writefln("medialist-cli (MediaList) %s", MediaListCLIVersion);
        writeln("Copyright (C) 2021-2023 dawning.");
        writeln("License GPLv3+: GNU GPL Version 3 or later <https://gnu.org/licenses/gpl.html>");
        writeln("This is free software: you are free to change and redistribute it.");
        writeln("There is NO WARRANTY, to the extent permitted by law.");
        return EXIT_SUCCESS;
    default:
        break;
    }

    /* medialist <command> <option> */
    if (args.length < 3) {
        display_usage(program_name);
        return EXIT_FAILURE;
    }

    command = args[1];
    mkdirRecurse(projectDirs.configDir);

    if (buildPath(projectDirs.configDir, "medialist.conf").exists) {
        scope contents = readText(buildPath(projectDirs.configDir, "medialist.conf"));
        auto conf = CniParser.fromString(contents, CniOptions(false, true));

        if ("general.directory" in conf) {
            trace("Using >=0.3 configuration section");
            dataDirectory = conf["general.directory"];
        } else if ("Lists.directory" in conf) {
            /*
             * COMPAT: For pre-0.2 versions of medialist.
             * REMOVE: At version 0.5
             */
            trace("Using pre-0.2 configuration format");
            stderr.writeln(`WARNING: Using the "Lists" section in your configuration is deprecated.`);
            stderr.writeln(`         Please change to using the "general" section.`);
            stderr.writeln(`         This warning (along with support for "Lists") will be removed v0.5`);
            dataDirectory = conf["Lists.directory"];
        }

        dataDirectory = expandEnvironmentVariables(expandTilde(dataDirectory));

        string shouldVerifyDeleteRaw;

        try {
            shouldVerifyDeleteRaw = conf["cli.verify_delete"];
        } catch (Exception e) {
            shouldVerifyDeleteRaw = conf.get("general.verify_delete", "true");
        }

        try {
            shouldVerifyDelete = parse!bool(shouldVerifyDeleteRaw);
        } catch (ConvException ce) {
            shouldVerifyDelete = true;
        }
    }

    mkdirRecurse(dataDirectory);

    if ("import" == command) {
        trace(`Command: import`);
        success = importList(dataDirectory, args[2..$]);
        return success ? EXIT_SUCCESS : EXIT_FAILURE;
    }

    /* temporary mess while changing to the library format */
    string filePath = buildPath(dataDirectory, args[2] ~ ".tsv");
    MediaList *ml = null;

    if (false == exists(filePath)) {
        if ("create" == command) {
            ml = ml_open_list(filePath);
            ml_free_list(ml);
            return EXIT_SUCCESS;
        }

        stderr.writefln(`List "%s" could not be found at "%s".`, args[2], filePath);
        return EXIT_FAILURE;
    }

    ml = ml_open_list(filePath);
    scope(exit) ml_free_list(ml);

    switch (command) {
    case "update": // CLI: <list> <id> [--title] [--status] [--progress] [--start-date] [--end-date]
        success = updateListItem(ml, args[3..$]);
        break;
    case "add": // CLI: <list> <title> [--status] [--progress]
        success = addToList(ml, args[3..$]);
        break;
    case "show": // CLI: [--numbered] [--show-headers]
        success = showList(ml, args[3..$]);
        break;
    case "delete": // CLI: <list> [<id...>]
        success = deleteFromList(ml, shouldVerifyDelete, args[3..$]);
        break;
    case "export": // CLI: <list> [--type]
        success = exportList(ml, args[3..$]);
        break;
    default:
        logf("Received unknown command: %s", command);
        display_usage(program_name);
        break;
    }

    return (true == success) ? EXIT_SUCCESS : EXIT_FAILURE;
}
