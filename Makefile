.POSIX:
.SUFFIXES:

DC = gdc
DFLAGS = -frelease -O
DTESTFLAGS = -fdebug -g -funittest -Wall

prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin

datarootdir = $(prefix)/share
datadir = $(datarootdir)
mandir = $(datarootdir)/man
man1dir = $(mandir)/man1

docdir = $(datarootdir)/doc/medialist
infodir = $(datarootdir)/info
pdfdir = $(docdir)

srcdir = .

MAKEINFO = makeinfo -o medialist.info --no-split
TEXI2PDF = makeinfo -o medialist.pdf --no-split --pdf

# Output flag for the D compiler.
__dof__ = -o 

ifeq ($(DC),ldc2)
	__dof__ = -of 
	DFLAGS = --release -O -I mlib/source
endif

ifeq ($(DC),dmd)
	__dof__ = -of=
	DFLAGS = -release -O -I=mlib/source/
endif

ifeq ($(DC),gdc)
	DFLAGS += -I mlib/source
endif

all: medialist-cli

medialist-cli: cni.o directories.o main.o medialist.o util.o trash.o
	$(DC) $(__dof__)medialist-cli cni.o directories.o main.o \
		medialist.o util.o trash.o

mostlyclean:
	rm -f *.o \
		docs/medialist.log docs/medialist.toc docs/medialist.cp \
		docs/medialist.cps docs/medialist.aux

clean: mostlyclean
	rm -f medialist-cli medialist-cli-unittest \
		medialist.pdf medialist.info

distclean: clean

install: medialist-cli installdirs
	cp -f medialist-cli $(DESTDIR)$(bindir)
	gzip < man/medialist-cli.1 > $(DESTDIR)$(man1dir)/medialist-cli.1.gz

installdirs: mkinstalldirs
	$(srcdir)/mkinstalldirs \
		$(DESTDIR)$(bindir) $(DESTDIR)$(datadir) \
		$(DESTDIR)$(infodir) $(DESTDIR)$(mandir) \
		$(DESTDIR)$(docdir)

uninstall:
	rm -f $(DESTDIR)$(bindir)/medialist-cli
	rm -f $(DESTDIR)$(man1dir)/medialist-cli.1.gz
	rm -f $(DESTDIR)$(infodir)/medialist.info
	rm -f $(DESTDIR)$(pdfdir)/medialist.pdf

unittest: cni-u.o directories-u.o medialist-u.o util-u.o \
		unittests-u.o tap-u.o trash-u.o
	$(DC) $(DTESTFLAGS) $(__dof__) medialist-cli-unittest $^

cni-u.o: mlib/source/mlib/cni.d
	$(DC) $(DTESTFLAGS) -c $(__dof__) $@ $<

directories-u.o: mlib/source/mlib/directories.d
	$(DC) $(DTESTFLAGS) -c $(__dof__) $@ $<

medialist-u.o: medialist.d
	$(DC) $(DTESTFLAGS) -c $(__dof__) $@ $<

tap-u.o: tap.d
	$(DC) $(DTESTFLAGS) -c $(__dof__) $@ $<

util-u.o: util.d
	$(DC) $(DTESTFLAGS) -c $(__dof__) $@ $<

unittests-u.o: unittests.d
	$(DC) $(DTESTFLAGS) -c $(__dof__) $@ $<

trash-u.o: mlib/source/mlib/trash.d
	$(DC) $(DTESTFLAGS) -c $(__dof__) $@ $<


check: unittest
	./medialist-cli-unittest

# mlib
cni.o: mlib/source/mlib/cni.d
	$(DC) $(DFLAGS) -c $<

directories.o: mlib/source/mlib/directories.d
	$(DC) $(DFLAGS) -c $<

trash.o: mlib/source/mlib/trash.d
	$(DC) $(DFLAGS) -c $<

# Documentation Related

info: medialist-info
pdf: medialist-pdf

medialist-info: docs/medialist.info
	$(MAKEINFO) $(srcdir)/docs/medialist.info

medialist-pdf: docs/medialist.info
	$(TEXI2PDF) $(srcdir)/docs/medialist.info

install-info: info mkinstalldirs
	$(srcdir)/mkinstalldirs $(DESTDIR)$(infodir)
	cp medialist.info $(DESTDIR)$(infodir)

install-pdf: pdf mkinstalldirs
	$(srcdir)/mkinstalldirs $(DESTDIR)$(pdfdir)
	cp medialist.pdf $(DESTDIR)$(pdfdir)


# Suffixes

.SUFFIXES: .d .o
.d.o:
	$(DC) $(DFLAGS) -c $<
