/*
 * Copyright (C) 2021, 2022 mio <stigma@disroot.org>
 *
 * This file is part of medialist-cli.
 *
 * medialist-cli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * medialist-cli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with medialist-cli.  If not, see <https://www.gnu.org/licenses/>.
 */
module util;

/**
 * Expand a string which can contain POSIX-style environment variables.
 *
 * This function does $(B NOT) expand tildes.
 */
@safe public string
expandEnvironmentVariables(string str)
{
	import std.array : appender;
	import std.process : environment;
	import std.string : indexOf, indexOfAny;

	ptrdiff_t startPos = 0;
	ptrdiff_t endPos = 0;

	auto result = appender!string;

	startPos = indexOf(str, '$', endPos);
	if (-1 == startPos)
	{
		return str;
	}

	do
	{
		result ~= str[endPos..startPos];

		endPos = indexOfAny(str, "/ ", startPos);

		/* Env. var ends the string */
		/* startPos+1 to remove the $ */
		if (endPos == -1)
		{
			result ~= environment.get(str[startPos+1..$], "");
			break;
		}
		else
		{
			result ~= environment.get(str[startPos+1..endPos], "");
		}

		startPos = indexOf(str, '$', endPos);

		if (startPos == -1)
		{
			result ~= str[endPos..$];
			break;
		}


	} while (true);

	return result.data;
}

///
@safe unittest
{
	import std.process : environment;

	/* Store previous paths */
	immutable prevHome = environment.get("HOME", null);
	immutable prevConf = environment.get("XDG_CONFIG_HOME", null);

	/* setup some example paths */
	immutable mlConfigPath = "$XDG_CONFIG_HOME/medialist";
	immutable mlDataDir = "$HOME/medialist";

	environment["HOME"] = "/home/username";
	environment["XDG_CONFIG_HOME"] = "/home/username/.config";

	assert (environment["HOME"] == "/home/username", "$HOME != /home/username");
	assert (environment["XDG_CONFIG_HOME"] == "/home/username/.config",
		"$XDG_CONFIG_HOME != /home/username/.config");

	assert (expandEnvironmentVariables(mlConfigPath) == "/home/username/.config/medialist",
		"Could not expand $XDG_CONFIG_HOME/medialist to /home/username/.config/medialist");
	assert (expandEnvironmentVariables(mlDataDir) == "/home/username/medialist",
		"Could not expand $HOME/medialist to /home/username/medialist");


	/* Restore previous paths */
	environment["HOME"] = prevHome;
	environment["XDG_CONFIG_HOME"] = prevConf;
}
